# Forum regler

## Regler

Inlägg och användarkonton som inte följer reglerna kommer att tas bort eller skrivas om. Klagomål skickas till jobtechdevelopment@arbetformedlingen.se

- Inlägg som bryter mot Sveriges lagar.
- Alt A. Inlägg som innehåller andra personuppgifter än skribentens egna måste ha personens godkännande och vara tydligt märkta i inlägget.
- Alt B. Inlägg får inte innehålla andra personuppgifter än skribentens egna.
- Inlägg ska vara relevanta och sakliga, dvs inte innehålla kränkningar, personangrepp, etc.
- Inlägg får inte innehålla material med copyright som skribenten inte äger.

## Om du ser ett problem

Om du ser ett problem, gör oss uppmärksamman på det genom att skicka ett meddelande till jobtechdevelopment@arbetformedlingen.se

Vänliga Hälsningar

JobTech Development
