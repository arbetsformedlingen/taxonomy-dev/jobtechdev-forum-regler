# CODE OF CONDUCT

This Code of Conduct represents the agreed behavioral standards and shared values that holds the JobTech Development ecosystem and its community together.

Jobtech Development is a division of Arbetsförmedlingen that offers APIs and open source technologies to the public with the goal of facilitating a well-functioning labor market.

Our ecosystem includes our websites, tools and products that we offer, repositories and community’s spaces that we use as well as all public and private channels both online and off.

Our community consists of public organizations, private institutions, civil society organizations, academia, individuals and everyone who would like to contribute to or benefit from a better labor market.

This Code of Conduct represent an extension of the ‘equality’ and ‘justness’ principles that governs Arbetsförmedlingen. Please visit https://arbetsformedlingen.se/for-arbetssokande/arbeta-i-sverige/jamstalldhet-och-diskriminering to know more about those principles.

We reserve the rights to remove access to the JobTech Development ecosystem from those who violate this Code of Conduct or Arbetsförmedlingen principles.

To make a complaint or to report a misconduct, please write to jobtechdev@arbetformedlingen.se.

## Your Participation Counts

The conversations we have here set the tone for everyone. Help us influence the future of this community by choosing to engage in discussions that make this forum an interesting place to be — and avoiding those that do not.

Let’s try to leave our park better than we found it.

## Improve the Discussion

Help us make this a great place for discussion by always working to improve the discussion in some way, however small. If you are not sure your post adds to the conversation, think over what you want to say and try again later.

The topics discussed here matter to us, and we want you to act as if they matter to you, too. Be respectful of the topics and the people discussing them, even if you disagree with some of what is being said.

One way to improve the discussion is by discovering ones that are already happening. Please spend some time browsing the topics here before replying or starting your own, and you’ll have a better chance of meeting others who share your interests.

## Be Agreeable, Even When You Disagree

You may wish to respond to something by disagreeing with it. That’s fine. But, remember to criticize ideas, not people . Please avoid:

Name-calling.

Ad hominem attacks.

Responding to a post’s tone instead of its actual content.

Knee-jerk contradiction.

Instead, provide reasoned counter-arguments that improve the conversation.

## If You See a Problem, Flag It

The JobTech Development-team have special authority; they are responsible for this forum. But so are you. With your help, the team can be community facilitators, not just janitors or police.

When you see bad behavior, don’t reply. It encourages the bad behavior by acknowledging it, consumes your energy, and wastes everyone’s time. Just flag it . If enough flags occur, action will be taken, either automatically or by the teams intervention.

In order to maintain our community, the JobTech Development-team reserve the right to remove any content and any user account for any reason at any time. To make a complaint or to report a misconduct, write to jobtechdev@arbetformedlingen.se clarifying which dialog didn’t respect the code and why.

## Always Be Civil

Nothing sabotages a healthy conversation like rudeness:

Be civil. Don’t post anything that a reasonable person would consider offensive, abusive, or hate speech.

Keep it clean. Don’t post anything obscene or sexually explicit.

Respect each other. Don’t harass or grief anyone, impersonate people, or expose their private information.

Respect our forum. Don’t post spam or otherwise vandalize the forum.

These are not concrete terms with precise definitions — avoid even the appearance of any of these things. If you’re unsure, ask yourself how you would feel if your post was featured on the front page of the New York Times.

This is a public forum, and search engines index these discussions. Keep the language, links, and images safe for family and friends.

    Please avoid using overtly sexual aliases or other nicknames that might detract from a friendly, safe and welcoming environment for all.
    Please be kind and courteous. There’s no need to be mean or rude.
    Respect that people have differences of opinion and that every design or implementation choice carries a trade-off and numerous costs. There is seldom a right answer.
    Please keep unstructured critique to a minimum. If you have solid ideas you want to experiment with, make a fork and see how it works.
    We will exclude you from interaction if you insult, demean or harass anyone. That is not welcome behavior.
    Private harassment is also unacceptable. No matter who you are, if you feel you have been or are being harassed or made uncomfortable by a community member, please contact one in the JobTech Development-team. Whether you’re a regular contributor or a newcomer, we care about making this community a safe place for you and we’ve got your back.
    Likewise any spamming, trolling, flaming, baiting or other attention-stealing behavior is not welcome.

## Post Only Your Own Stuff

You may not post anything digital that belongs to someone else without permission. You may not post descriptions of, links to, or methods for stealing someone’s intellectual property (software, video, audio, images), or for breaking any other law.https://www.rust-lang.org/policies/code-of-conduct

Excellent :slight_smile:

